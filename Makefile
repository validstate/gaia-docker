build:
	./build.sh

clean:
	rm -rf go/
	rm -rf testnets/
	rm -rf artifacts/
	rm -f *.swp *.swo

push:
	./push.sh
