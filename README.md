A setup for running sentry nodes on a one-node Kubernetes cluster.

* `./build.sh` generates a Docker image with a totally static gaia binary (version configured in `config`)
* `./publish.sh` pushes it to this [GitLab registry](https://gitlab.com/validstate/gaia-docker/container_registry) (you need to export `GITLAB_USER` and `GITLAB_API_TOKEN` for authn)
* the `manifests/` run this image with a `gaia-2` testnet configuration

To change the testnet: modify the ConfigMap  
To deploy: `kubectl apply -f manifests/`  
To peek: `kubectl exec gaia-2-0 -it sh`  

# Prerequisites:
* directory `/var/lib/gaia-2-home` on the worker node
* DNS record `rpc.gaia-2.cosmos.valid.st` pointing the the worker node