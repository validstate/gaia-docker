#!/bin/bash -e

source config
source gitlab-access-token

docker login registry.gitlab.com -u ${GITLAB_USER} -p ${GITLAB_API_TOKEN}
docker push ${IMAGE_TAG_GITLAB}

docker push ${IMAGE_TAG_QUAY}

