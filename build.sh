#!/bin/bash -e

source config

proj_dir="$(pwd)"
rm -rf go
mkdir -p go
rm -rf artifacts
mkdir -p artifacts

export GOPATH="${proj_dir}/go"
export PATH="${GOPATH}/bin:${PATH}"

set +e
go get github.com/cosmos/cosmos-sdk
set -e

cd ${GOPATH}/src/github.com/cosmos/cosmos-sdk
#git fetch --all
git checkout "${COSMOS_GIT_TAG}"
#git checkout 9d90c6b
make get_tools
make get_vendor_deps

make install
#make install_examples

#CGO_ENABLED=0 GOOS=linux go build -a -o ${proj_dir}/artifacts/gaia ./cmd/gaia
CGO_ENABLED=0 GOOS=linux go build -a -o ${proj_dir}/artifacts/gaiad-static ./cmd/gaiad

cd ${proj_dir}
strip artifacts/gaiad-static

docker build . -t ${IMAGE_TAG_GITLAB}
docker build . -t ${IMAGE_TAG_QUAY}
